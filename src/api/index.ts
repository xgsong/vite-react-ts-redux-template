export interface IPersonDto {
  id: number;
  name: string;
  age: number;
}

export const fetchPerson = async (): Promise<IPersonDto> => {
  await new Promise((resolve) => setTimeout(resolve, 1000));
  return {
    id: 4,
    name: "Billy Song",
    age: 36,
  };
};
