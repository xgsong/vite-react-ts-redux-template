import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface AccountState {
  name: string;
  amount: number;
}

const initialState: AccountState = {
  name: "Billy Song",
  amount: 99.45,
};

export const accountSlice = createSlice({
  name: "account",
  initialState,
  reducers: {
    increment: (state) => {
      state.amount += 1;
    },
    decrement: (state) => {
      state.amount -= 1;
    },
    incrementByAmount: (state, action: PayloadAction<number>) => {
      state.amount += action.payload;
    },
    decrementByAmount: (state, action: PayloadAction<number>) => {
      state.amount -= action.payload;
    },
  },
});

export const { increment, decrement, incrementByAmount, decrementByAmount } =
  accountSlice.actions;

//export const selectAmount = (state: RootState) => state.account.amount;
//export const selectName = (state: RootState) => state.account.name;

export default accountSlice.reducer;
