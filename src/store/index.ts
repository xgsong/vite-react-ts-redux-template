import { configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import createSagaMiddleware from "redux-saga";
import accountSlice from "./features/accountSlice";
import rootSaga from "./sagas";

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: {
    account: accountSlice,
  },
  middleware: [sagaMiddleware] as const,
});

sagaMiddleware.run(rootSaga);

export default store;
