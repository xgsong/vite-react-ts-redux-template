import { delay, fork, cancel, call } from "@redux-saga/core/effects";
import { PayloadAction } from "@reduxjs/toolkit";

function* forkTask() {
  try {
    yield delay(2000);
    console.log("forkTask finished");
  } catch (e) {
    console.log("error: ", e);
  } finally {
    console.log("cleanup code.");
  }
}

function* cancelFork() {
  const task = yield fork(forkTask);
  yield delay(1000);
  yield cancel(task);
}

function* rootSaga() {
  yield call(cancelFork);
}

export default rootSaga;
