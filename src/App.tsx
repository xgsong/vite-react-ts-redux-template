import logo from "./logo.svg";
import "./App.css";
import {
  decrement,
  decrementByAmount,
  increment,
  incrementByAmount,
} from "./store/features/accountSlice";
import { useAppDispatch, useAppSelector } from "./store";

function App() {
  const { name, amount } = useAppSelector((state) => state.account);
  const dispatch = useAppDispatch();

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Hello Vite + React!</p>
        <p>Account Name: {name}</p>
        <p>Account Amount: ${amount}</p>
        <button onClick={() => dispatch(increment())}>Add $1</button>
        <button onClick={() => dispatch(decrement())}>Minus $1</button>
        <button onClick={() => dispatch(incrementByAmount(5))}>Add $5</button>
        <button onClick={() => dispatch(decrementByAmount(5))}>Minus $5</button>
      </header>
    </div>
  );
}

export default App;
